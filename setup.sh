sudo pacman -Syu gcc
chsh -s /usr/bin/zsh
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
cd ..
rm -rf yay
cd $HOME
yay -Syu
yay -S antigen-git neovim rcm deoplete-git
env RCRC=$HOME/dotfiles/rcrc rcup
cd $HOME
rcup -d $HOME/dotfiles
mkdir -p $HOME/.config/nvim
ln -s $HOME/dotfiles/vimrc $HOME/.config/nvim/init.vim

yay -S fnm 
eval "$(fnm env)"
fnm install --lts



#docker
yay -S docker-bin
sudo systemctl start docker.service
sudo systemctl enable docker.service
sudo groupadd docker
sudo usermod -aG docker $USERNAME
yay -S docker-compose

yay -S termite i3blocks i3blocks-contrib

#yubikey
yay -S gnupg pcsclite opensc ccid hopenpgp-tools yubikey-personalization yubikey-oath-dmenu
gpg --keyserver keyserver.ubuntu.com --recv 69F17729
sudo systemctl start pcscd.service 
sudo systemctl enable pcscd.service

#fonts
yay -S ttf-google-fonts-git noto-fonts-emoji ttf-font-awesome

#apps
yay -S google-chrome firefox visual-studio-code-bin spotify neovim-plug franz-bin flameshot  notion-app watson jq slack-desktop bat kubectl tig

#pulseaudio
yay -S pulseaudio pulseaudio-alsa pavucontrol 
systemctl --user enable pulseaudio.service
systemctl --user enable pulseaudio.socket
systemctl --user start pulseaudio.service
systemctl --user status pulseaudio.{service,socket}


