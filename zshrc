source /opt/homebrew/share/antigen/antigen.zsh
setopt autocd
# setopt complete_aliases
#
autoload -Uz compinit promptinit
compinit
promptinit

zstyle :compinstall filename '/home/daniel/.zshrc'
zstyle ':completion:*' menu select
zstyle ':completion:*' rehash true

ZSH_THEME="lambda-mod"
EDITOR="/usr/bin/nvim"
export GPG_TTY="$(tty)"
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
gpgconf --launch gpg-agent



# Load the oh-my-zsh's library.
antigen use oh-my-zsh

antigen bundle z
antigen bundle fzf
antigen bundle aws 
antigen bundle docker
antigen bundle git
antigen bundle gitfast
antigen bundle kubectl 
antigen bundle npm
antigen bundle vi-mode
antigen bundle yarn 
antigen bundle watson 
antigen bundle "MichaelAquilina/zsh-you-should-use"


antigen bundle zsh-users/zsh-completions
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle zsh-users/zsh-syntax-highlighting

if [ $commands[oc] ]; then
  source <(oc completion zsh)
fi

# Load the theme.
antigen theme halfo/lambda-mod-zsh-theme lambda-mod

# Tell Antigen that you're done.
antigen apply

bindkey ^r fzf-history-widget
bindkey ^f fzf-file-widget
bindkey '^ ' autosuggest-accept

source ~/.aliases
source ~/.profile






[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

export BYOBU_PREFIX=/usr/local



function reveal {
  output=$(echo "${1}" | rev | cut -c16- | rev)
  gpg --decrypt --output ${output} "${1}" \
    && echo "${1} -> ${output}" }

function secret {  # list preferred id last
  output="${HOME}/$(basename ${1}).$(date +%F).enc"
  gpg --encrypt --armor \
    --output ${output} \
    -r 0x0FF5114269F17729 \
    -r daniel@schulz.codes \
    "${1}" && echo "${1} -> ${output}" }


  export NVM_DIR="$HOME/.nvm"
  [ -s "/opt/homebrew/opt/nvm/nvm.sh" ] && \. "/opt/homebrew/opt/nvm/nvm.sh"  # This loads nvm
  [ -s "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm" ] && \. "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion


export PATH="~/.bin:$PATH"

export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"
export PATH="/opt/homebrew/opt/openjdk/bin:$PATH"
